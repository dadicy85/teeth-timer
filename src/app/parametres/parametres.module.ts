import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Parametres } from './parametres.page';

import { ParametresRoutingModule } from './parametres-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ParametresRoutingModule
  ],
  declarations: [Parametres]
})
export class ParametresModule {}
