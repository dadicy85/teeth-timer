import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Parametres } from './parametres.page';

const routes: Routes = [
  {
    path: '',
    component: Parametres,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametresRoutingModule {}
