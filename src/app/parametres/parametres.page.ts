import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Preferences } from '@capacitor/preferences';
import { ViewWillEnter } from '@ionic/angular';

@Component({
  selector: 'app-parametres',
  templateUrl: 'parametres.page.html',
  styleUrls: ['parametres.page.scss'],
})
export class Parametres implements ViewWillEnter {
  settingsForm!: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.settingsForm = this.formBuilder.group({
      totalZones: [15, Validators.required],
      zoneTime: [16, Validators.required],
      zoneBreak: [6, Validators.required],
      childName: [
        'Mon petit Marco',
        [Validators.required, Validators.maxLength(20)],
      ],
    });
  }

  ionViewWillEnter() {}

  async setValue() {
    await Preferences.set({
      key: 'totalZones',
      value: this.settingsForm.controls['totalZones'].value,
    });
    await Preferences.set({
      key: 'zoneTime',
      value: this.settingsForm.controls['zoneTime'].value,
    });
    await Preferences.set({
      key: 'zoneBreak',
      value: this.settingsForm.controls['zoneBreak'].value,
    });
    await Preferences.set({
      key: 'childName',
      value: this.settingsForm.controls['childName'].value,
    });
  }

  saveSettings() {
    if (this.settingsForm.invalid) return;
    this.setValue();
  }
}
