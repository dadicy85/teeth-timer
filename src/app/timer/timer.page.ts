import { Component } from '@angular/core';
import { Preferences } from '@capacitor/preferences';

@Component({
  selector: 'app-timer',
  templateUrl: 'timer.page.html',
  styleUrls: ['timer.page.scss'],
})
export class Timer {
  brushingZones: number = 0;
  tempsRestant: string = '00:00';
  childName!: string;
  totalZones!: number;
  zoneBreak!: number;
  zoneTime!: number;

  private timer: any;
  private tempsActuel!: number;
  enPause: boolean = false;

  constructor() {}

  ionViewWillEnter(): void {
    this.getAllValues();
  }

  private async getAllValues(): Promise<void> {
    const totalZones = (await Preferences.get({ key: 'totalZones' })).value;
    const zoneTime = (await Preferences.get({ key: 'zoneTime' })).value;
    const zoneBreak = (await Preferences.get({ key: 'zoneBreak' })).value;
    const childName = (await Preferences.get({ key: 'childName' })).value!;

    if (totalZones) {
      this.totalZones = parseInt(totalZones, 10);
    }
    if (zoneTime) {
      this.zoneTime = parseInt(zoneTime, 10);
    }
    if (zoneBreak) {
      this.zoneBreak = parseInt(zoneBreak, 10);
    }
    if (childName) {
      this.childName = childName;
    }
  }

  pauseTimer() {
    this.enPause = true;
    this.tempsActuel = this.zoneBreak;
    this.timer = setInterval(() => {
      this.tempsActuel--;
      this.tempsRestant = this.formatTime(this.tempsActuel);
      if (this.tempsActuel === 0) {
        clearInterval(this.timer);
        this.brushingZones++;
        this.playTimer();
      }
    }, 1000);
  }

  playTimer() {
    if (this.enPause) {
      this.enPause = false;
    }
    if (this.brushingZones < this.totalZones) {
      this.tempsActuel = this.zoneTime;
      this.timer = setInterval(() => {
        this.tempsActuel--;
        this.tempsRestant = this.formatTime(this.tempsActuel);

        if (this.tempsActuel == 0) {
          clearInterval(this.timer);
          this.pauseTimer();
        }
      }, 1000);
    }
    if(this.brushingZones >= this.totalZones) {
      clearInterval(this.timer);
      this.tempsRestant = 'Brossage Terminé';
    }
  }

  resetTimer() {
    this.pauseTimer();
    this.tempsActuel = this.zoneTime;
    this.tempsRestant = this.formatTime(this.tempsActuel);
    this.brushingZones = 0;
  }

  formatTime(temps: number): string {
    let minutes: number = Math.floor(temps / 60);
    let secondes: number = temps - minutes * 60;

    let minutesString: string = minutes.toString();
    let secondesString: string = secondes.toString();

    if (minutes < 10) {
      minutesString = '0' + minutesString;
    }

    if (secondes < 10) {
      secondesString = '0' + secondesString;
    }

    return minutesString + ':' + secondesString;
  }
}
