import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Timer } from './timer.page';

import { TimerRoutingModule } from './timer-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TimerRoutingModule
  ],
  declarations: [Timer]
})
export class TimerModule {}
