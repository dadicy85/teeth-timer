import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Timer } from './timer.page';

const routes: Routes = [
  {
    path: '',
    component: Timer,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimerRoutingModule {}
